From 4b10a9b6ba299f804f7c24e9d455e6b6997a9eb4 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Zbigniew=20J=C4=99drzejewski-Szmek?= <zbyszek@in.waw.pl>
Date: Sun, 16 Oct 2022 12:54:34 +0200
Subject: [PATCH] TEST-15: also test hierarchical drop-ins for slices

Slices are worth testing too, because they don't need a fragment path so they
behave slightly differently than service units. I'm making this a separate
patch from the actual tests that I wanted to add later because it's complex
enough on its own.

(cherry picked from commit f80c874af376052b6b81f47cbbc43d7fecd98cd6)

Related: #2156620
---
 test/TEST-15-DROPIN/test-dropin.sh | 37 +++++++++++++++++++++++++++---
 1 file changed, 34 insertions(+), 3 deletions(-)

diff --git a/test/TEST-15-DROPIN/test-dropin.sh b/test/TEST-15-DROPIN/test-dropin.sh
index bcd351360d..d46946fd77 100755
--- a/test/TEST-15-DROPIN/test-dropin.sh
+++ b/test/TEST-15-DROPIN/test-dropin.sh
@@ -128,8 +128,8 @@ EOF
     clear_units {a,b,c}.service
 }
 
-test_hierarchical_dropins () {
-    echo "Testing hierarchical dropins..."
+test_hierarchical_service_dropins () {
+    echo "Testing hierarchical service dropins..."
     echo "*** test service.d/ top level drop-in"
     create_services a-b-c
     check_ko a-b-c ExecCondition "/bin/echo service.d"
@@ -153,6 +153,36 @@ ExecCondition=/bin/echo $dropin
     clear_units a-b-c.service
 }
 
+test_hierarchical_slice_dropins () {
+    echo "Testing hierarchical slice dropins..."
+    echo "*** test slice.d/ top level drop-in"
+    # Slice units don't even need a fragment, so we test the defaults here
+    check_ok a-b-c.slice Description "a-b-c.slice"
+    check_ok a-b-c.slice MemoryMax "infinity"
+
+    # Test drop-ins
+    for dropin in slice.d a-.slice.d a-b-.slice.d a-b-c.slice.d; do
+        mkdir -p /usr/lib/systemd/system/$dropin
+        echo "
+[Slice]
+MemoryMax=1000000000
+        " >/usr/lib/systemd/system/$dropin/override.conf
+        systemctl daemon-reload
+        check_ok a-b-c.slice MemoryMax "1000000000"
+        rm /usr/lib/systemd/system/$dropin/override.conf
+    done
+
+    # Test unit with a fragment
+    echo "
+[Slice]
+MemoryMax=1000000001
+        " >/usr/lib/systemd/system/a-b-c.slice
+    systemctl daemon-reload
+    check_ok a-b-c.slice MemoryMax "1000000001"
+
+    clear_units a-b-c.slice
+}
+
 test_template_dropins () {
     echo "Testing template dropins..."
 
@@ -338,7 +368,8 @@ test_invalid_dropins () {
 }
 
 test_basic_dropins
-test_hierarchical_dropins
+test_hierarchical_service_dropins
+test_hierarchical_slice_dropins
 test_template_dropins
 test_alias_dropins
 test_masked_dropins
