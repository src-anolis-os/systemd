From e7f656192ea399043947640a664bd94129e9549b Mon Sep 17 00:00:00 2001
From: Eric DeVolder <eric.devolder@oracle.com>
Date: Mon, 13 Apr 2020 16:22:04 -0500
Subject: [PATCH] pstore: introduce tmpfiles.d/systemd-pstore.conf

The systemd pstore service archives the contents of /sys/fs/pstore
upon boot so that there is room for a subsequent dump.  The issue is
that while the service is present, the kernel still needs to be
configured to write data into the pstore. The kernel has two
parameters, crash_kexec_post_notifiers and printk.always_kmsg_dump,
that control writes into pstore.

The crash_kexec_post_notifiers parameter enables the kernel to write
dmesg (including stack trace) into pstore upon a panic, and
printk.always_kmsg_dump parameter enables the kernel to write dmesg
upon a shutdown (shutdown, reboot, halt).

As it stands today, these parameters are not managed/manipulated by
the systemd pstore service, and are solely reliant upon the user [to
have the foresight] to set them on the kernel command line at boot, or
post boot via sysfs. Furthermore, the user would need to set these
parameters in a persistent fashion so that that they are enabled on
subsequent reboots.

This patch introduces the setting of these two kernel parameters via
the systemd tmpfiles technique.

(cherry picked from commit f00c36641a253f4ea659ec3def5d87ba1336eb3b)

Resolves: #2211416
---
 man/systemd-pstore.service.xml | 18 ++++++++++++++++++
 tmpfiles.d/meson.build         |  1 +
 tmpfiles.d/systemd-pstore.conf | 29 +++++++++++++++++++++++++++++
 3 files changed, 48 insertions(+)
 create mode 100644 tmpfiles.d/systemd-pstore.conf

diff --git a/man/systemd-pstore.service.xml b/man/systemd-pstore.service.xml
index 47916da521..335a3b3d18 100644
--- a/man/systemd-pstore.service.xml
+++ b/man/systemd-pstore.service.xml
@@ -81,6 +81,24 @@
       <citerefentry><refentrytitle>pstore.conf</refentrytitle><manvolnum>5</manvolnum></citerefentry>.
       </para>
     </refsect2>
+
+    <refsect2>
+      <title>Controlling kernel parameters</title>
+
+      <para> The kernel has two parameters,
+      <filename>/sys/module/kernel/parameters/crash_kexec_post_notifiers</filename> and
+      <filename>/sys/module/printk/parameters/always_kmsg_dump</filename>,
+      that control writes into pstore.
+      The crash_kexec_post_notifiers parameter enables the kernel to write
+      dmesg (including stack trace) into pstore upon a panic or crash, and
+      printk.always_kmsg_dump parameter enables the kernel to write dmesg
+      upon a normal shutdown (shutdown, reboot, halt). These kernel
+      parameters are managed via the
+      <citerefentry><refentrytitle>tmpfiles.d</refentrytitle><manvolnum>5</manvolnum></citerefentry>
+      mechanism, specifically the file <filename>/usr/lib/tmpfiles/systemd-pstore.conf</filename>.
+      </para>
+    </refsect2>
+
   </refsect1>
 
   <refsect1>
diff --git a/tmpfiles.d/meson.build b/tmpfiles.d/meson.build
index 35eea2be5c..5ebb8f432a 100644
--- a/tmpfiles.d/meson.build
+++ b/tmpfiles.d/meson.build
@@ -7,6 +7,7 @@ tmpfiles = [['home.conf',            ''],
             ['systemd-nologin.conf', ''],
             ['systemd-nspawn.conf',  'ENABLE_MACHINED'],
             ['portables.conf',       'ENABLE_PORTABLED'],
+            ['systemd-pstore.conf',  'ENABLE_PSTORE'],
             ['tmp.conf',             ''],
             ['x11.conf',             ''],
             ['legacy.conf',          'HAVE_SYSV_COMPAT'],
diff --git a/tmpfiles.d/systemd-pstore.conf b/tmpfiles.d/systemd-pstore.conf
new file mode 100644
index 0000000000..cb600ec1ee
--- /dev/null
+++ b/tmpfiles.d/systemd-pstore.conf
@@ -0,0 +1,29 @@
+# SPDX-License-Identifier: LGPL-2.1+
+#
+# The systemd-pstore.service(1) archives the contents of /sys/fs/pstore
+# upon boot so that there is room for a subsequent dump. This service
+# is enabled with:
+#  systemctl enable systemd-pstore
+#
+# With the service enabled, the kernel still needs to be configured
+# to write data into the pstore. The kernel has two parameters,
+# crash_kexec_post_notifiers and printk.always_kmsg_dump, that
+# control writes into pstore.
+#
+# The crash_kexec_post_notifiers parameter enables the kernel to write
+# dmesg (including stack trace) into pstore upon a panic, and
+# printk.always_kmsg_dump parameter enables the kernel to write dmesg
+# upon a normal shutdown (shutdown, reboot, halt).
+#
+# To configure the kernel parameters, uncomment the appropriate
+# line(s) below. The value written is either 'Y' to enable the
+# kernel parameter, or 'N' to disable the kernel parameter.
+#
+# After making a change to this file, do:
+#  systemd-tmpfiles --create path/to/tmpfiles.d/systemd-pstore.conf
+#
+# These changes are automatically applied on future re-boots.
+
+d /var/lib/systemd/pstore 0755 root root 14d
+#w /sys/module/printk/parameters/always_kmsg_dump - - - - Y
+w /sys/module/kernel/parameters/crash_kexec_post_notifiers - - - - Y
