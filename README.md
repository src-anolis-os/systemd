# systemd-239

This is the repository of systemd-239 for Anolis OS 8.

## Patch index convention

Below is the patch index convention of this repository:

- 0001  ... 0xxx  : patches from upstream srpm
- 10001 ... 10xxx : patches cherry-picked from systemd github upstream
- 20001 ... 20xxx : original patch by OpenAnolis community
- 910001 ... 910xxx : LifseaOS patches that cherry-picked from systemd github upstream
- 920001 ... 920xxx : LifseaOS original patches
