From 9f181efdd59bd3e9134cf94007953562ca8b57fa Mon Sep 17 00:00:00 2001
From: Lennart Poettering <lennart@poettering.net>
Date: Sat, 15 Dec 2018 12:25:32 +0100
Subject: [PATCH] fileio: when reading a full file into memory, refuse inner
 NUL bytes

Just some extra care to avoid any ambiguities in what we read.

(cherry picked from commit beb90929913354eec50c3524086fe70d14f97e2f)

Signed-off-by: Guorui Yu <GuoRui.Yu@linux.alibaba.com>
---
 src/basic/fileio.c        | 25 +++++++++++++++++++------
 src/test/test-unit-file.c | 10 +++++-----
 2 files changed, 24 insertions(+), 11 deletions(-)

diff --git a/src/basic/fileio.c b/src/basic/fileio.c
index 733fb42463..9fef97ff0c 100644
--- a/src/basic/fileio.c
+++ b/src/basic/fileio.c
@@ -383,16 +383,20 @@ int read_full_virtual_file(const char *filename, char **ret_contents, size_t *re
         return 0;
 }
 
-int read_full_stream(FILE *f, char **contents, size_t *size) {
+int read_full_stream(
+                FILE *f,
+                char **ret_contents,
+                size_t *ret_size) {
+
         _cleanup_free_ char *buf = NULL;
         struct stat st;
         size_t n, l;
         int fd;
 
         assert(f);
-        assert(contents);
+        assert(ret_contents);
 
-        n = LINE_MAX;
+        n = LINE_MAX; /* Start size */
 
         fd = fileno(f);
         if (fd >= 0) { /* If the FILE* object is backed by an fd (as opposed to memory or such, see fmemopen(), let's
@@ -448,11 +452,20 @@ int read_full_stream(FILE *f, char **contents, size_t *size) {
                 n = MIN(n * 2, READ_FULL_BYTES_MAX);
         }
 
+        if (!ret_size) {
+                /* Safety check: if the caller doesn't want to know the size of what we just read it will rely on the
+                 * trailing NUL byte. But if there's an embedded NUL byte, then we should refuse operation as otherwise
+                 * there'd be ambiguity about what we just read. */
+
+                if (memchr(buf, 0, l))
+                        return -EBADMSG;
+        }
+
         buf[l] = 0;
-        *contents = TAKE_PTR(buf);
+        *ret_contents = TAKE_PTR(buf);
 
-        if (size)
-                *size = l;
+        if (ret_size)
+                *ret_size = l;
 
         return 0;
 }
diff --git a/src/test/test-unit-file.c b/src/test/test-unit-file.c
index 09b0179fa1..e64a27dd39 100644
--- a/src/test/test-unit-file.c
+++ b/src/test/test-unit-file.c
@@ -532,7 +532,7 @@ static void test_load_env_file_1(void) {
 
         fd = mkostemp_safe(name);
         assert_se(fd >= 0);
-        assert_se(write(fd, env_file_1, sizeof(env_file_1)) == sizeof(env_file_1));
+        assert_se(write(fd, env_file_1, strlen(env_file_1)) == strlen(env_file_1));
 
         r = load_env_file(NULL, name, NULL, &data);
         assert_se(r == 0);
@@ -554,7 +554,7 @@ static void test_load_env_file_2(void) {
 
         fd = mkostemp_safe(name);
         assert_se(fd >= 0);
-        assert_se(write(fd, env_file_2, sizeof(env_file_2)) == sizeof(env_file_2));
+        assert_se(write(fd, env_file_2, strlen(env_file_2)) == strlen(env_file_2));
 
         r = load_env_file(NULL, name, NULL, &data);
         assert_se(r == 0);
@@ -571,7 +571,7 @@ static void test_load_env_file_3(void) {
 
         fd = mkostemp_safe(name);
         assert_se(fd >= 0);
-        assert_se(write(fd, env_file_3, sizeof(env_file_3)) == sizeof(env_file_3));
+        assert_se(write(fd, env_file_3, strlen(env_file_3)) == strlen(env_file_3));
 
         r = load_env_file(NULL, name, NULL, &data);
         assert_se(r == 0);
@@ -586,7 +586,7 @@ static void test_load_env_file_4(void) {
 
         fd = mkostemp_safe(name);
         assert_se(fd >= 0);
-        assert_se(write(fd, env_file_4, sizeof(env_file_4)) == sizeof(env_file_4));
+        assert_se(write(fd, env_file_4, strlen(env_file_4)) == strlen(env_file_4));
 
         r = load_env_file(NULL, name, NULL, &data);
         assert_se(r == 0);
@@ -605,7 +605,7 @@ static void test_load_env_file_5(void) {
 
         fd = mkostemp_safe(name);
         assert_se(fd >= 0);
-        assert_se(write(fd, env_file_5, sizeof(env_file_5)) == sizeof(env_file_5));
+        assert_se(write(fd, env_file_5, strlen(env_file_5)) == strlen(env_file_5));
 
         r = load_env_file(NULL, name, NULL, &data);
         assert_se(r == 0);
-- 
2.39.1

